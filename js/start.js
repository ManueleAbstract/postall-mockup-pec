jQuery(function ($) {
  console.log($("#content").outerHeight());
  $('#mailList tr').each(function(){
    if($(this).hasClass('unread')){
      $(this).css("font-weight", "bold");
    }
  });

  $("#treeOptions").on('hidden.bs.collapse', function () {
    $("#treeOptions-plus-minus").removeClass("fa-minus").addClass("fa-plus");
  }).on('shown.bs.collapse', function () {
    $("#treeOptions-plus-minus").removeClass("fa-plus").addClass("fa-minus");
  });

  $("#mailList th input[type='checkbox']").bind("click", function(){
    if($(this).prop("checked")){
      select("tutti");
    }
    else {
      select ("nessuno");
    }
  })

  addVericalSplitter();

  $('#mailList tbody tr').click(function(){
    addHorizontalSplitter();
    $(this).removeClass("unread");
    $(this).css("font-weight", "normal");

    $('#singleMail').removeClass("hidden");
    var index = $(this).data("val");
    $("#mailBody").text(mails[index].content);
    $("#header1").html("<div><strong>POSTA CERTIFICATA: "+mails[index].subject+"</strong><br/>"+
                        "From: "+mails[index].from+"<br/>"+
                        "To:"+currentMail+"</div");
    $("#header2").html("<div><strong>"+mails[index].subject+"<br/></strong>"+
                        "From: "+mails[index].from+"<br/>"+
                        "To:"+currentMail+"</div");
  });



  /*
  Decommentare se si vogliono caricare dinamicamente (load di jQuery) gli htmlSnippets
  $("#header").load("header.html");
  $("#footer").load("footer.html");
  $("#mailOptions").load("mailOptionInbox.html");

  var activeOption = $("#inbox");
  $("#inbox").addClass("active");

  //Option bindings
  $("#inbox").bind("click", function(){
    $("#mailOptions").load("mailOptionInbox.html");
    toggleActiveOption(activeOption, $(this));
  });
  $("#draft").bind("click", function(){
    $("#mailOptions").load("mailOptionDraft.html");
    toggleActiveOption(activeOption, $(this));
  });
  $("#sent").bind("click", function(){
    $("#mailOptions").load("mailOptionSent.html");
    toggleActiveOption(activeOption, $(this));
  });
  $("#trash").bind("click", function(){
    $("#mailOptions").load("mailOptionTrash.html");
    toggleActiveOption(activeOption, $(this));
  });

  function toggleActiveOption(oldOpt, newOpt){
    if(oldOpt != newOpt){
      oldOpt.removeClass("active");
      newOpt.addClass("active");
      activeOption = newOpt;
    }
  }
*/

  var activeOption = $("#mailOptionInbox");
  //Option bindings
  $("#inbox").bind("click", function(){
    toggleVisiblePanel(activeOption, $("#mailOptionInbox"));
  });
  $("#sent").bind("click", function(){
    toggleVisiblePanel(activeOption, $("#mailOptionSent"));
  });
  $("#draft").bind("click", function(){
    toggleVisiblePanel(activeOption, $("#mailOptionDraft"));
  });
  $("#trash").bind("click", function(){
    toggleVisiblePanel(activeOption, $("#mailOptionTrash"));
  });

  function toggleVisiblePanel(oldPan, newPan){
    if(oldPan.prop("id") !== newPan.prop("id")){
      newPan.removeClass("hidden");
      oldPan.addClass("hidden");
      activeOption = newPan;
    }
  }
});

function select(val){
  switch(val){
    case 'tutti':
      $("#mailList input[type='checkbox']").prop("checked", true);
      break;
    case 'non-letti':
      $("#mailList input[type='checkbox']").prop("checked", false);
      $("#mailList tr[class='unread'] input[type='checkbox']").prop("checked", true);
      break;
    case 'nessuno':
      $("#mailList input[type='checkbox']").prop("checked", false);
      break;
  }
}

//info splitter: http://janfield.ca/github/jquery.enhsplitter/demo.html
function addVericalSplitter(){
  var totalHeight = $('#content').outerHeight();
  $("#menuPanel").css("height", totalHeight);
  var totalWidth = $('#menuPanel').outerWidth();
  $('#menuPanel').enhsplitter({position:"20%", leftMinSize:"0", rightMinSize:(totalWidth/2), splitterSize:"8px", handle:"dots", collapse:"left", vertical: true});
}
function addHorizontalSplitter(){
  //var totalHeight = $('#mailPanel').outerHeight();
  $('#mailPanel').enhsplitter({splitterSize:"8px", handle:"dots", collapse:"down", vertical: false});
}
